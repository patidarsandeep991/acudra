/*
SQLyog Enterprise - MySQL GUI v8.14 
MySQL - 5.5.24 : Database - acudra
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`acudra` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `acudra`;

/*Table structure for table `acudra_registration` */

DROP TABLE IF EXISTS `acudra_registration`;

CREATE TABLE `acudra_registration` (
  `registration_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `company` varchar(150) DEFAULT NULL,
  `address1` varchar(200) DEFAULT NULL,
  `address2` varchar(200) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `postCode` varchar(30) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `subscribe` tinyint(1) DEFAULT NULL,
  `privacyPolicy` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`registration_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `acudra_registration` */

insert  into `acudra_registration`(`registration_id`,`name`,`email`,`telephone`,`fax`,`company`,`address1`,`address2`,`city`,`postCode`,`country`,`password`,`subscribe`,`privacyPolicy`) values (1,'sandeep','sandeep@gmail.com','8585858588','085858','aartek','indore','indore','indore','8585855','india','12345',0,0),(8,'Acudra','acudra@gmail.com','696969696969','25858585','Acudra','India','mumbai','indore','85858585','1','12345',1,1),(9,'sdfsd','sdfs','3242','234234','dsfsdf','sdfsdf','sdfs','sdfs','23424','1','23123',1,1),(10,'jariya','sandeep@jariya.com','966969696','858585','Aartek','India','DEwas','churlay','452001','3','sandeep',1,1);

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(55) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `country` */

insert  into `country`(`country_id`,`country_name`) values (1,'India'),(2,'pakistan'),(3,'Bangladesh'),(4,'Bhutan'),(5,'shriLanka'),(6,'England'),(7,'Rassia'),(8,'Hindustan');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(55) DEFAULT NULL,
  `password` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `NewIndex1` (`username`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
