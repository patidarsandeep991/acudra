package com.acudra.service;

import java.util.List;

import com.acudra.model.Country;
import com.acudra.model.Registration;

public interface LoginService {

	public Boolean customerLogin(Registration registration);

	public List<Country> getCountryName();
}
